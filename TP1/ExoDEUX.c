

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<getopt.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
  "

void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}



void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);  
}



char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);
    
    // Checking if ERRNO is set
    if (str == NULL) 
      perror(strerror(errno));
  }

  return str;
}



static struct option binary_opts[] = 
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { "output",  required_argument, 0, 'o' },
  { 0,         0,                 0,  0  } 
};


const char* binary_optstr = "hvi:o:";




int main(int argc, char** argv)
{
  
  short int is_verbose_mode = 0;
  char* bin_input_param = NULL;
  char* bin_output_param = NULL;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();         
        }
        break;
      case 'o':
        //output param
        if (optarg)
        {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);
 
        exit(EXIT_SUCCESS);
      default :
        break;
    }
  } 

  
  if (bin_input_param == NULL)
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }


  // Printing params
 dprintf(1, "** PARAMS **\n%-8s: %s\n%-8s: %d\n",
          "input",   bin_input_param,
          "verbose", is_verbose_mode);


  // Business logic must be implemented at this point

   printf("\n** OUTPUT **\n");
  int desc_input = open(bin_input_param, O_RDWR);

  if(desc_input < 0){
    perror("Votre fichier ne contient aucun caractère !");
    exit(EXIT_FAILURE);
  }

  // on positionne le curseur à la fin du document pour avoir sa taille
  off_t size = lseek(desc_input, 0, SEEK_END);

  // on crée un tableau avec juste ce qu'il faut comme espace (comme on lit caractère par caractère, buffer de 1)
  unsigned char buffer[1];

  off_t i;
  for(i=size-1; i>= 0; i--){
    lseek(desc_input, i, SEEK_SET);

    // on lit le document jusqu'à la fin
    if(read(desc_input,buffer,1) != 1)  return 1;
    // on affiche le resultat
    
    fprintf( stdout,buffer,"%s");

  }

 printf("\n");
  close(desc_input);

  // Freeing allocated data
  free_if_needed(bin_input_param);


  return EXIT_SUCCESS;
}

