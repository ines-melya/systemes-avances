#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

int scorePere;
int scoreFils;
void p1sig(int);
void p2sig(int);

void Fils(int);
void Pere(int);
int loupe();

int main()
{
	int pidFils = fork();

	if ( pidFils == 0 )
	{
		Fils(pidFils);
	}
	else
	{
		Pere(pidFils);
	}

	return 0;
}

void Pere(int pidFils)
{
	struct sigaction sa;
	sigset_t sigmask;
	sa.sa_handler = p1sig;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	sigaction(SIGUSR1, &sa, NULL);

	while(1)
	{
		
		sigfillset(&sigmask);
		sigdelset(&sigmask,SIGUSR1);
		sigdelset(&sigmask,SIGINT);
		sigsuspend(&sigmask);
		sleep(1);
		kill(pidFils, SIGUSR1);
		if (scorePere == 13)
		{
			fprintf(stderr,"j’ai gagné %d\n",getpid());
			kill(pidFils,SIGTERM);
			exit(EXIT_SUCCESS);
		}		
		else
		{
			scorePere += loupe();
		}
	}
}

void Fils(int pidFils)
{
	struct sigaction sa;
	sigset_t sigmask;

	sa.sa_handler = p2sig;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	sigaction(SIGUSR1, &sa, NULL);

	while(1)
	{
		
		sleep(1);
		kill(getppid(), SIGUSR1);

		sigfillset(&sigmask);
		sigdelset(&sigmask,SIGUSR1);
		sigdelset(&sigmask,SIGINT);
		sigsuspend(&sigmask);
		
		if (scoreFils == 13)
		{
			fprintf(stderr,"j’ai gagné %d\n",getpid());
			kill(pidFils - 1,SIGTERM);
			exit(EXIT_SUCCESS);
		}
		else
		{
			scoreFils += loupe();
		}
	}
}

void p1sig(int signo)
{
	fprintf(stderr, "Ping... P: %d\n",scorePere);
}

void p2sig(int signo)
{
	fprintf(stderr, "Pong... F : %d\n",scoreFils);
}

int loupe()
{
	return rand() % 2;
}