#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>

/**
 * L'objectif de ce code est de mettre en place d'un processus :
 * ===
 * - générant deux processus fils de même niveau hiérarchique.
 * - réalisant une communication ascendante fils -> père via les tubes.
 * ===
 * Schématiquement :
 * ===
 *       Père
 *       ^ ^
 *       | |
 * Fils1 | | Fils2
 */
int main(int argc, char** argv) {
  int process_son_pid = fork();
  int process_son_return_code;
  int son = getpid();
  int father = getppid();

  if (process_son_pid == -1) {
      perror("fork");
      exit(EXIT_FAILURE);
  }
  if (process_son_pid == 0) {
      // code executé dans le fils

      printf("PID FILS : %d\n", getpid());
      printf("PID PERE : %d\n", getppid());

      exit(getpid());

     
  }
  // affiche pid du fils
  printf("FORK SON : %i\n", process_son_pid);

  //if ((0 < waitpid(son, &process_son_return_code, 0)) && (WIFEXITED(process_son_return_code))){
     // printf("Le code de retour est %d\n", WEXITSTATUS(process_son_return_code));
  //}
  // attend la fin du processus fils, recupere le code de retour dans son_code_result
  wait(&process_son_return_code);

  printf("Code de retour du fils : %i\n", process_son_return_code / 256);
    
  return 0;
}
