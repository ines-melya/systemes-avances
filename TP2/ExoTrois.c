#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

#define STDIN 0
#define STDOUT 1
#define STDERR 2


int main(int argc, char **argv)
{
  //GOAL : ps eaux | grep "^root " > /dev/null && echo "root est connecté"

  if (argc != 1)
  {
    dprintf(STDERR, "Bad usage!\n");
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }

  // SON A (create by father): execute command ps eaux, create SON B and send ps result to it throught PIPE A
  // SON B : execute command grep "^root" on ps result and throw result in /dev/null
  // FATHER : execute command "echo 'root est connecté'" if everthing went all right

  int pipe_A[2];
  pipe(pipe_A);

  int result_pipe_A;

  if(fork() == 0){
    // CODE EXECUTED BY SON

    if (fork() == 0)
    {
      // CODE EXECUTED BY SON B

      // # read ps result
      // read in pipe A
      close(pipe_A[1]);

      // redirect stdout to read result in pipe A
      dup2(pipe_A[0], STDIN);

      // open /dev/null
      int fd;
      if ((fd = open("/dev/null", O_WRONLY)) < 0)
      {
        perror(strerror(errno));
        exit(1);
      }

      dup2(fd, STDOUT);

      // #2 grep "^root"

      if (execlp("grep", "grep", "^root", NULL) < 0)
      {
        perror(strerror(errno));
        exit(1);
      }

      exit(0);
    }
    else
    {
      // CODE EXECUTED BY SON A (FATHER OF B)

      // #1 ps eaux

      // write in pipe A : to send result
      close(pipe_A[0]);

      // redirect stdout to write result in pipe_A
      dup2(pipe_A[1], STDOUT);

      // Execute command ps eaux
      if (execlp("ps", "ps", "eaux", NULL) < 0)
      {
        perror(strerror(errno));
        exit(1);
      }

      wait(NULL);
      exit(0);
    }
  }
  else{
    // CODE EXECUTED BY FATHER
    close(pipe_A[0]);
    close(pipe_A[1]);

    // wait for son A who's waiting for son B
    wait(&result_pipe_A);

    if (WEXITSTATUS(result_pipe_A) == 0){
      // #3 echo
      write(1, "root est connecté\n", 19);
    }
  }

  return 0;
}
