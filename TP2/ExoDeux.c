#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char **argv)
{
    const int STDOUT = 1;
    const int STDERR = 2;
    char* param = NULL;

    if (argc == 1)
    {
        dprintf(STDERR,"%s\n","error");
        exit(EXIT_FAILURE);
    }
    else if (argc > 1)
    {
        param = argv[1];
        dprintf(STDOUT, "%s : %s\n", argv[0], param);
    }

    if(fork() == 0)
    {
            int fileDesc = 0;

            char tempFileName[] = "proc-exercise";
            printf("PID fils : %d\n", getpid());
            close(STDOUT);

            if ((fileDesc = mkstemp(tempFileName)) == -1)
            {
                perror("impossible de créer le fichier temporaire");
                exit(EXIT_FAILURE);
            }
            dup(fileDesc);
            printf("New descripteur : %d\n", fileDesc);
            
            int execv_result = 0;
            if((execv_result = execv(param, argv)) == -1){
                perror("error");
                exit(EXIT_FAILURE);
            }
        }
        else{
            // code execute par le pere
            printf("PID pere : %d\n", getpid());
            wait(NULL);

            printf("Je suis le PID Père\n");
        }

        return 0;
}